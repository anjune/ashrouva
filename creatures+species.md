# What is the Ashrouva?

## Fae creatures and species of the dying earth

### Mimetic Imps (aka Mimes, Mockers)

Vaguely monkey-like fae creatures, standing a stooped 1.30m tall, with coarse rust-coloured fur, sharp claws, pointy little teeth, long tufted tails, and unsteady and unfocused small round black eyes. Must have escaped earlier than “our” goblins. Needing the same nourishment of (typically human) creativity and inspiration as those (recall the faery habit of abducting poets and stealing human babies) but lacking their intelligence, the hardy *Mimes* or *Mockers* have long lost their minds to intellectual starvation and are now most of the time stuck aping whatever last caught their attention without any apparent rhyme or reason. The goblins (themselves deteriorating) sometimes train them for simple repetitive tasks, but generally ignore them. They can be a bit embarrassing, you see? 

They serve several functions in the story and/or game:
* unreliable living surveillance cameras, providing clues or betraying our heroes
* an image of the goblins’ own future
* unsettling, humiliating buzzkills

### Lightning Sprites (aka Terrible Angels, Wraiths)

Known to humankind as a high-altitude atmospheric phenomenon (see pictures). Translucent, autumn-tinted, fairy-like apparitions with “wings” like plasma discharges licking at and pushing against the ground or walls to provide lift when coming down on earth. Some like leaving a vaguely leaf-shaped afterimage of these “wings”, and the plasma filaments are the veins in those leaves. Less often, they will make a conscious effort to become solid to the point of having discernible clothing or facial features and interact with the goblins.

“Our” uneducated goblins fear them as ghosts or reapers, as the emergence of a new Sprite is always tied to the apparent death of a goblin. In reality, they are simply the goblin’s adult stage — but they won’t communicate that clearly because:

* They are generally out of touch with mundane creatures such as their own long-sleeping offspring
* It was typically late-stage *Ashrouvan* goblins who were encouraging their own onto the path, not the Sprites themselves
* Maturation into a Sprite depends on the goblin developing an often painful *dysphoric* desire for advancement (in fae terms) and glamour *without* a promise of it ever paying off (?)

(*Terrible Angels* is Lilleveer’s affectionate, awestruck term for them; *Wraiths* is more typical goblin terminology.)

### Goblins

Pre-Sprites. Come in various shapes and sizes and colours: Fox Clan, Badger Clan, Hare Clan, etc.; settlements in the City tend to be heterogenous (goblins of different clans live together). 

#### “Our” goblins

* Named characters: Lilleveer. Zierfa. Ammerin. Tilza. Mjuven.

#### Beyond the pale 

Goblins who embraced and exploited and explored the decently well preserved human technology found in their area of the City (in what used to be Eurasian tundra). Initially a solid boost toward a complex civilisation, this approach has by now nudged them more than halfway down the path to becoming more advanced and dangerous mimetic imps. The closest we have to an urban civilisation now, but really rather a bizarre dysfunctional parody of one. 

* This is not an anti-technology or anti-city screed; the degeneration happens due to the sheer concentration of "necrotic" human inspiration these goblins absorb.
* They run a madhouse for Ashrouvans, so their last sane people as well as captured ones they’re "helping" conveniently tend to all end up in one place. 

### Cats

Other, more elegant, pre-Sprites who’re often envious of how quickly goblins *can* metamorphise if they’re not scared out of it. Might not constitute a different species of fae so much as merely a clan unique enough to be listed separately. Known to explore the city rather than stay put. 

* They can be travelling traders and entertainers.
* Named characters: “Doctor” Ennif. 

### Fae Deer

Graceful white creatures that can occasionally be seen foraging (or putting on an imitation of it?) in the luminescent fungal forests that are springing up in the City's former parks, agricultural sectors, dried-up waterways, coastlines.

## Mutant future animals and plants 

There’s nothing left that we would recognise. Photosynthesis has become impossible and with it the oxygen-exuding, sustenance-providing plant life supporting higher animals. What appears to be land flora is typically closely related to corals, jellies, polyps, molluscs. Bioluminescence is common on the darkened dying earth. It can be quite beautiful, if you aren’t just creeped out completely.

### "Fungal zombies"

Start out looking like ordinary mould but, incorporating the decaying host's DNA, can end up as a puffy, fuzzy, imperfect copy with a brief disturbing life… scattering spores and degrading back beyond recognition.

* An old immortality experiment run out of control?

### Luminescent forests

A variety of fungi have started to grow large enough to form small forests. Whether their bioluminescence was at some point engineered into them or used to serve to attract spore-spreading creatures or creatures that would provide sustenance once dead of toxins is not known.

* Graceful stalks with a scintillating dangling globe containing a fluid with spores. Globe can be used as lamp when hanged securely and replaced before it will burst.
* Tall pale funnels growing in groups.

### Spongy carpet liform

Found mostly where water used to be; pale and squishy. Prolonged exposure will irritate naked feet and cause painful swellings. Otherwise decently comfortable bed stuffing provided one has trustworthy enough blankets and bedding.

## Machines

### Surveyors

Leftover drones, many “dead”, some still making a nuisance of themselves. Capable of forming swarms, which increases each individual drone’s intelligence. Many are donut- or horseshoe-shaped with a variety of external sensors and thrusters, some agile bicopters that can navigate narrow alleys and tunnels, some ornithopters for higher altitudes. Rarest are globes and cubes with antigravity devices; those have some use as “pets” or toys for goblins. 

* Drones are still occasionally used as intended (i.e. surveillance or exploration) Beyond the Pale.
* “Pets” can turn into spies.

### Transport 

Low-altitude transport, originally filling roles comparable to a van or minibus depending on configuration.

* One is used to abduct Lilleveer to Beyond the Pale.
