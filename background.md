# What is the Ashrouva?

## What is “What is the Ashrouva?”?

* It’s a horrible working title lifted from the viral marketing (?) around the movie The Matrix (“What is the Matrix?”) that had come out at/around that time. The *Ashrouva* is a similarly mysterious concept to the protagonist (but one that has nothing to do with the Matrix at all and instead originates in a dream I had).

* It’s a fairy story set in the future, with science fiction *elements* that a reader can pick up on, but not science fiction per se nor scientific.

* It might take the form of a series of dreamlike or prose-poëmic vignettes, at least until I can somehow learn how to think in stories and have characters with actual lives run the show coherently.

* It *started* as a tale of anthropomorphic animals; now we have slightly theriomorphised ‘goblin’ clans instead.

* It was originally meant to become a text adventure game, with the main challenge being the translation of Lilleveer’s dreamed rituals into the more mundane terms of the game world. (The classical example: you need a chalice, *but there is no chalice*. What's *like* a chalice? A badly rusted discarded tin can. The lesson: The attitude and trust in what you’re doing must come from you — it cannot be contributed by the tools except during the very first steps of the way.)

## The backstory

Far into the future, Earth is the wealthy political centre of an interplanetary empire. Its fabled ancient cities have melded into one proud megacity. The duchies and counties of Europe and the Eurasian subarctic (the setting of the story) are famous above all for their fusion of sky-scraping gothic and art nouveau architecture and decor and the ubiquitous stained glass…

*Even farther* into the future, that empire has lost control of itself. Its unknown new masters have renamed it the *Sagittarius Hegemony*, with Earth lying in that constellation from *their* perspective. Most of Earth’s energy and engineering ingenuity is directed at the protection of the former jewel of the empire from its own aging sun…

Much, much later still: earth around the time of the story is a quiet, gloomy planet lying abandoned under a swollen red sun. Jupiter, too, hangs huge in the sky — some colossal engineering project must have moved Earth’s orbit outward in the past. Earth seems to be home to scant few hardy mutant fauna and flora only, slowly reclaiming the planet-spanning twilit city…

…until the frequent natural disasters caused by the “upset” climate and all that desperately ambitious re-orbiting disrupt the *fairy folk*'s epochs-spanning underground sleep (they had long ago decided to wait for less *populous* times). Most of them never make it out — but many of their youngest emerge alone and confused and pastless into a desolate directionless world. Some go feral — but some latch on to fragments of an all but dead, dusty human culture instead and resettle far-flung neighbourhoods of the megacity. All the space in between remains an unlit wilderness of imposing architectures, sparsely populated by slow sullen bioluminescent mutant wildlife and feral fae creatures, and with upon layer of history waiting to be uncovered…
